#!/usr/bin/env pwsh

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

# get component data and set necessary variables
$package = Get-Content -Path "package.json" | ConvertFrom-Json
$buildImage="$($package.name):$($package.version)-build"
$container=$package.name

# create .npmrc with proper npm token 
if (-not (Test-Path -Path "docker/.npmrc")) {
    Copy-Item -Path "~/.npmrc" -Destination "docker"
}

# build docker image
docker build -f docker/Dockerfile -t $buildImage .

# remove container if it exists
if (docker ps -q -a -f name=$container) {
    docker rm $container --force
}
# create and copy compiled files, then destroy
docker run --name $container $buildImage sh -c $package.scripts."build"
# check is build successfull
if ($LastExitCode -ne 0) {
    exit 1
}
docker cp "$($container):/app/dist" ./dist
docker rm $container
