import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CustomButtonModule } from '../../projects/lib-sample-angular/src/lib/custom-button/custom-button.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CustomButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
