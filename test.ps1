#!/usr/bin/env pwsh

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

# get package data and set necessary variables
$package = Get-Content -Path "package.json" | ConvertFrom-Json
$testImage="$($package.name):$($package.version)-test"
$container=$package.name

# create .npmrc with proper npm token 
if (-not (Test-Path -Path "docker/.npmrc")) {
    Copy-Item -Path "~/.npmrc" -Destination "docker"
}

# build docker image
Write-Host "before docker init"
docker build -f docker/Dockerfile -t $testImage .

# remove container if it exists
if (docker ps -q -a -f name=$container) {
    docker rm $container --force
}
# run tests
docker run --name $container $testImage sh -c $package.scripts."test:ci"

# check if test was successfull
$logs = docker logs $container
docker rm $container
$testResult = $logs[$logs.Count - 1]
if ($testResult -notmatch "^TOTAL: [0-9]+ SUCCESS$") {
    Write-Host "Some test failed.`n$testResult"
    exit 1
}
